@extends('ui.master')
@section('content')
    <div class="custom-product">
        <div class="col-sm-12">
            <div class=""> 
                <a href="/guitar">Go Back</a>
                <h2>Cart List</h2>
                <div class="">
                    @foreach ($data as $item)
                        <div class="row search-item cart-list-devider">
                            <div class='col-sm-4'>
                                <a href="pdetail/{{$item->id}}">
                                    <img class='cart-img' src="{{$item->gallery}}">
                                </a>
                            </div>
                            <div class='col-sm-4'>
                                    <div class="">
                                        <h2>{{$item->name}}</h2>
                                        <h5>{{$item->description}}</h5>
                                    </div>
                            </div>
                            <div class='col-sm-4'>
                                <a href="/removecart/{{$item->cart_id}}" class='btn btn-warning'>Remove from Cart</a>
                            </div>
                            </div>
                        </div> 
                    @endforeach
                    <a class='btn btn-success' href='/ordernow'>Order Now</a> <br> <br>
                </div>
            </div> 
        </div>
    </div>
@endsection


