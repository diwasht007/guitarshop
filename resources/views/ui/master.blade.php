<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-comm Project</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
     {{-- {{View::make('ui.header')}} --}}
    @include('ui.header')
    @yield('content')
    @include('ui.footer')
    {{-- {{View::make('ui.footer')}} --}}


    

</body>
<style>
    .custom-login{
        height: 500px;
        padding-top: 100px;

    }
    img.slider-img{
        height: 400px !important;
    }
    .custom-product{
        height: 600px;
    }
    .slider-text{
        background-color: rgb(182 166 166 / 8%);
    }
    .image-detail{
        height: 200px;
    }
    .searched-item{
        height: 100px;
    }
    .cart-list{
        float: left;
        width: 20%
    }
    .cart-img{
        height: 100px;
    }
    .cart-wrapper{
        margin: 25px;
    }
    .cart-list-devider{
        border-bottom: 1px solid #cccccc;
        margin-bottom: 10px;
        padding-bottom: 10px;
    }
    .trending-item{
        float: left;
        width: 20%;
    }
    .Trending-wrapper{
        margin: 25px;
    }
    .detail-img{
        height: 200px; 
    }
    .search-box{
        width: 500px !important;
    }
</style>
    
</html>