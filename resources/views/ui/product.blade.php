@extends('ui.master')
@section('content')
    <div class="custom-product">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                @foreach ($guitar as $item)
                    <div class="item {{ $item['id'] == 1 ? 'active' : '' }}">
                        <a href="guitar/{{ $item['id'] }}">
                            <img class='slider-img' src="{{ $item['gallery'] }}" alt="pic">
                            <div class="carousel-caption slider-text">
                                <h3>{{ $item['name'] }}</h3>
                                <p>{{ $item['description'] }}</p>
                            </div>
                        </a>

                    </div>
                @endforeach

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    @if (Session::has('user'))
        <div class="col-sm-12">
            <div class="">
                <h2>Guitar List</h2>
                <div class="">
                    @foreach ($guitar as $item)
                        <div class="row search-item cart-list-devider">
                            <div class='col-sm-4'>
                                <a href="pdetail/{{ $item->id }}">
                                    <img class='cart-img' src="{{ $item->gallery }}">
                                </a>
                            </div>
                            <div class='col-sm-4'>
                                <div class="">
                                    <h2>{{ $item->name }}</h2>
                                    <h5>{{ $item->description }}</h5>
                                    <h5>{{ $item->type }}</h5>
                                    <h5>{{ $item->price }}</h5>
                                    <h5>{{ $item->reamaining_quantity }}</h5>
                                </div>
                            </div>
                            <div class='col-sm-4'>
                                <form action="{{ route('guitar.destroy', $item->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class='btn btn-warning'>Delete Guitar</button>
                                </form>
                            </div>
                        </div>
                </div>
                @endforeach
                <a class='btn btn-success' href='/ordernow'>Order Now</a> <br> <br>
            </div>
        </div>

    @endif
    </div>
@endsection
