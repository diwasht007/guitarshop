@extends('ui.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img class="image-detail" src="{{ $guitar['gallery'] }}" alt="">
            </div>
            <div class="col-sm-6">
                <a href="/guitar">Go back</a>
                <h2>Name: {{ $guitar['name'] }}</h2>
                <h3>Price: {{ $guitar['price'] }}</h3>
                <h4>Type: {{ $guitar['type'] }}</h4>
                <h4>Description: {{ $guitar['description'] }}</h4>
                <h4>Warranty: {{ $guitar['Warranty'] }}</h4>
                <h4>Available Stock: {{ $guitar['reamaining_quantity'] }}</h4>
                <br><br>
                <form action="/addtocart" method="POST">
                    <input type="hidden" name="guitar_id" value="{{$guitar['id']}}">
                    @csrf
                    <button class="btn btn-success ">Add to Cart</button>
                </form>
                <br><br>
                <button class="btn btn-primary ">Buy Now</button>
                <br><br>
            </div>
        </div>
    </div>
@endsection
