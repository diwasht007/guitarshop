@extends('ui.master')
@section('content')
    <div class="custom-product">
        <div class="col-sm-10">
            <div class="">
                <a href="/guitar">Go Back</a>
                <h2>My Orders</h2>
                <div class="">
                    @foreach ($data as $item)
                        <div>
                            <div class="row search-item cart-list-devider">
                                <div class='col-sm-4'>
                                    <a href="/guitar {{ $item->id }}">
                                        <img class='cart-img' src="{{ $item->gallery }}">
                                    </a>
                                </div>
                                <div class='col-sm-4'>
                                    <div class="">
                                        <h2>{{ $item->name }}</h2>
                                        <h5>{{ $item->price }}</h5>
                                        <h5>{{ $item->type }}</h5>
                                        <h5>{{ $item->description }}</h5>
                                        <h5>{{ $item->order_status }}</h5>
                                        <h5>Uder ID {{ $item->user_id }}</h5>
                                        <h5>{{ $item->id }}</h5>
   
                                    </div>
                                </div>
                                <div class='col-sm-4'>
                                    <a href="/removeorder/{{$item->id}}" class='btn btn-warning'>Remove from order</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
