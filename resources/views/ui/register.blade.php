@extends('ui.master')
@section('content')
<div class="container custom-login">
    <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
    
    @if ($errors->any())
  
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach


    @endif
        <form action='register' method='POST'>
            @csrf
            <div class="form-group">
            <label for="name" class="form-label">Name</label>
            <input type="text" name="name" placeholder="Enter your name" class="form-control" id="name">
            </div>
            <div class="form-group">
            <label for="exampleInputEmail1" class="form-label">Email address</label>
            <input type="email" name="email" placeholder="Enter your email" class="form-control" id="exampleInputEmail1">
            </div>
            <div class="form-group">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="password" name="password" placeholder="Enter your password" class="form-control" id="exampleInputPassword1">
            </div>
            <br>
            <button type="submit" class="btn btn-primary">submit</button>
        </form>
    </div>
    </div>
</div>
    
@endsection