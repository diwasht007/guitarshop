@extends('ui.master')
@section('content')
    <div class="custom-product">
        <div class="col-sm-6">
            <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Price</td>
                    <td> $ {{$data}} </td>
                  </tr>
                  <tr>
                    <td>Tax</td>
                    <td> $ {{$data/100}} </td>
                  </tr>
                  <tr>
                    <td>Delivery</td>
                    <td>$ 20</td>
                  </tr>
                    <td>Total Amount</td>
                    <td>$ {{$data+20}}</td>
                  </tr>
                </tbody>
            </table>

              <form method='POST' action="orderplace">
                @csrf
                <div class="form-group">
                  <textarea placeholder='enter your address' name='address' class="form-control"> </textarea>
                </div>
                <div class="form-group">
                    <label for="">Payment Method</label>
                  <p><input type="radio" value="cash" name="payment"> <span> Online Payment</span></p>
                  <p><input type="radio" value="cash" name="payment"> <span> Efts On Delivery</span></p>
                  <p><input type="radio" value="cash" name="payment"> <span> Cash on Delivery Payment</span></p>
                </div>
                <button type="submit" class="btn btn-default">Order Now</button>
              </form>

        </div>
    </div>
@endsection