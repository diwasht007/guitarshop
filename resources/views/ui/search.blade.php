@extends('ui.master')
@section('content')
    <div class="container custom-product">
        <div class="row">
            <div class="col-sm-6">
                @foreach ($data as $item)
                    <a href="guitar/{{ $item['id'] }}">
                        <img class="searched-item" src="{{ $item['gallery'] }}" alt="">
                        <div class="">
                            <h4>{{ $item['name'] }}</h4>
                            <h4>{{ $item['description'] }}</h4>
                        </div>
                    </a>
                @endforeach
            </div>       
        </div>      
    </div>
@endsection
