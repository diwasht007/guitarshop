@extends('ui.master')
@section('content')
    <!------ Include the above in your HEAD tag ---------->
    {{-- @if ($errors->any())
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    @endif --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div> 
        <form class="form-horizontal" action="{{ route('guitar.store') }}" method='POST'>
            @csrf
            <fieldset>
                <!-- Form Name -->
                <legend>PRODUCTS</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">PRODUCT NAME</label>
                    <div class="col-md-4">
                        <input id="product_name" name="name" placeholder="PRODUCT NAME" class="form-control input-md"
                            type="text">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">PRODUCT TYPE</label>
                    <div class="col-md-4">
                        <input id="type" name="type" placeholder="PRODUCT TYPE" class="form-control input-md"
                            type="text">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">PRODUCT PRICE</label>
                    <div class="col-md-4">
                        <input id="price" name="price" placeholder="price" class="form-control input-md"
                            type="text">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="total_quantity">AVAILABLE QUANTITY</label>
                    <div class="col-md-4">
                        <input id="total_quantity" name="total_quantity" placeholder="AVAILABLE QUANTITY"
                            class="form-control input-md" type="text">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Warranty">PRODUCT WARRANTY</label>
                    <div class="col-md-4">
                        <input id="Warranty" name="Warranty" placeholder="PRODUCT WEIGHT" class="form-control input-md"
                            type="text">

                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">PRODUCT DESCRIPTION</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                </div>


                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="comment">Gallery</label>
                    <div class="col-md-4">
                        <input id="gallery" name="gallery" placeholder="insert image link" class="form-control input-md"
                            type="text">

                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton">Single Button</label>
                    <div class="col-md-4">
                        <button id="singlebutton" name="singlebutton" class="btn btn-primary">Add Product</button>
                    </div>
                </div>

            </fieldset>
        </form>


    </div>
@endsection




<!-- File Button -->
{{-- <div class="form-group">
  <label class="col-md-4 control-label" for="filebutton">main_image</label>
  <div class="col-md-4">
    <input id="filebutton" name="filebutton" class="input-file" type="file">
  </div>
</div> --}}
