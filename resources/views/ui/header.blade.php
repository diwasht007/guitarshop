@php
    use App\Http\Controllers\Web\CartController;
    $total=0;
    if(Session::has('user')){
        $total = CartController::cartItem();
    }
    
@endphp
<nav class="navbar navbar-light">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/guitar">GuitarShop</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/guitar">Home</a></li>
        </ul>
        <form action='/search' class="navbar-form navbar-left" action="/action_page.php">
            <div class="form-group">
                <input type="text" name='query' class="form-control search-box" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Search</button>
        </form>
        <ul class="nav navbar-nav navbar-right">
            @if (Session::has('user'))
            <li><a href="/store">Add Guitars</a></li>
            <li><a href="/myorder">orders</a></li>
            <li><a href="/cartlist">cart({{ $total }})</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ Session::get('user')['name'] }}
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </li>
            @else
                <li><a href="/login">Login</a></li>
                <li><a href="/register">register</a></li>
            @endif
        </ul>
    </div>
</nav>
