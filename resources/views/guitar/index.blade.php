@extends('master')
@section('content')
<div class="container">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Type</th>
            <th scope="col">Description</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($guitar as $key => $item)
            <tr>
                <th scope="row">{{++$key}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->type}}</td>
                <td>{{$item->description}}</td>
              </tr>          
            @endforeach
          </tr>
        </tbody>
      </table>
</div>
@endsection

    

