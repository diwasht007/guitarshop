   <!-- Bootstrap -->
   <link href="{{ asset('/css/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
   <link href="{{ asset('/css/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
   <link href="{{ asset('/css/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
   <link href="{{ asset('/css/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
   <link href="{{ asset('/css/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}"
       rel="stylesheet">
   <link href="{{ asset('/css/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet">
   <link href="{{ asset('/css/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
   <link href="{{ asset('/css/build/css/custom.min.css') }}" rel="stylesheet">