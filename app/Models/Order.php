<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model

{
    use HasFactory;
    const PENDING= 'pending';
    const PAID= 'paid';
    const CANCELLED= 'cancelled';
    const RETURNED= 'returned';
    
    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function guitar(){
        return $this->belongsTo(Guitar::class, 'guitar_id', 'id');
    }
    public function coupons(){
        return $this->belongsToMany(Coupon::class,'coupon_order');
    }
}
