<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Coupon extends Model
{
    use HasFactory;


    public static function getDiscountAmount($coupon_code,$totalAmount)
    {
        $coupon= Coupon::where('coupon_code',$coupon_code)->first();
        if ($coupon->discount_type == 'fixed')  return $coupon->discount;
        return ($coupon->discount/100)*$totalAmount;
    }

    public static function isCouponValid($coupon_code){
        $coupon= Coupon::where('coupon_code',$coupon_code)->first();
        $totalUsedCoupon = $coupon->orders->count();
        if(!$coupon) return false;
        if(!$coupon->expiry_date >= Carbon::now())  return false;
        if($coupon->orders->where('user_id',Auth::id())->first()) return false;
        if(!($totalUsedCoupon < $coupon->max_users)) return false;
        return true;
    }

    public function orders(){
        return $this->belongsToMany(Order::class,'coupon_order');
    }
}
