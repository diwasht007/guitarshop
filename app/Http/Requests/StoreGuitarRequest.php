<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;


class StoreGuitarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|max:30|min:3',
            'type' => 'required',
            'description' => 'required',
            'Warranty' => 'required',
            'price' => 'required',
            'total_quantity' => 'required',
            'gallery' => 'required'
        ];
    }
    // protected function formatErrors(Validator $validator)
    // {
    //     return $validator->errors()->all();
    // }
}
