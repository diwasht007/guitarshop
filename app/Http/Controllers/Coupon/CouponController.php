<?php

namespace App\Http\Controllers\Coupon;

use App\Constants\ServerMessage;
use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;


class CouponController extends Controller
{
    public function addCoupons(Request $request){
     
       try{
        $coupon = new Coupon();
        $coupon->coupon_code =mt_rand(100000,999999);
        $coupon->discount = $request->discount;
        $coupon->discount_type = $request->discount_type;
        $coupon->max_amount = $request->max_amount;
        $coupon->max_users = $request->max_users;
        $coupon->expiry_date = Carbon::now()->addDays(5);
        $coupon->status = $request->status;
        $coupon->save();
       }catch(Exception $e){
        return response()->json(['error'=>$e->getMessage(), ServerMessage::SERVER_ERROR]);
       }
       return response()->json(['data'=>$coupon,'message'=>'your order has been placed'],ServerMessage::SUCESS);
    }

    public function showAllCoupons(){
        try{
            $coupon = Coupon::all();
            }
        catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()], ServerMessage::SERVER_ERROR);
        }
        return response()->json(['Coupons'=>$coupon], ServerMessage::SUCESS);
    }
}
