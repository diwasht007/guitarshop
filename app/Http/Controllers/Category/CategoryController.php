<?php

namespace App\Http\Controllers\Category;

use App\Constants\ServerMessage;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parentCategories = Category::where('parent_id', 0)->get();
       
        $this->getCategories($parentCategories);
        return response()->json(['Data' => $parentCategories], ServerMessage::SUCESS);
    }

    private function getCategories($parentCategories)
    {
        
        foreach ($parentCategories as $category)  {
            if($category->subCategory){ // no need of this line
                  $category->sub_category = $category->subCategory; // no need of this line
                 $this->getCategories($category->subCategory); 
            }
            // $sub = $this->getChildCategory($category->id);
            // return $category = Arr::add($category, 'sub_category', $sub);
        }
    }

    // private function getChildCategory($id)
    // {
    //     $categories = Category::where('parent_id', $id)->get();
    //     return $this->getCategories($categories);
    // }

    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($request->hasFile('category_image')) {
                $file = $request->file('category_image');
                $name = time() . '.' . $file->getClientOriginalExtension();
                $destination_path = public_path('images/category');
                $file->move($destination_path, $name);
            }
            $categories = new Category();
            $categories->title = $request->title;
            if ($request->parent_id == "") {
                $categories->parent_id = 0;
            } else $categories->parent_id = $request->parent_id;
            $categories->description = $request->description;
            if (isset($name)) {
                $categories->category_image = $request->$name;
                $categories->image_link = $request->image_link;
            }
            $categories->save();
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], ServerMessage::SERVER_ERROR);
        }
        return response()->json(['data' => $categories, 'message' => 'Category Added!!'], ServerMessage::SUCESS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //  $category = Category::where('id', $id)->first();
        // $products = $category->products()->orderBy('created_at')->paginate(12);

        // return response()->json(['products' => $products,'category' => $category], ServerMessage::SUCESS);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
