<?php

namespace App\Http\Controllers\Order;

use App\Constants\ServerMessage;
use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Guitar;
use App\Models\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function placeorder(Request $request)
    {
        $validator = Validator::make($request->only('guitar_id'), [
            'guitar_id' => 'required'
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json(['error' => $error], ServerMessage::VALIDATION_ERROR);
        }
        try {
            // $try= Order::where('id',15)->first();
        
            $guitar = Guitar::findorfail($request->guitar_id);

            if ($request->coupon_code) {
                $couponCode = $request->coupon_code;
                $coupon= Coupon::where('coupon_code',$couponCode)->get();
                
                if (Coupon::isCouponValid($couponCode)) {
                    $dicountAmount = Coupon::getDiscountAmount($couponCode, $guitar->price);
                }
            }
            $order = new Order();
            $order->user_id = Auth::guard('api')->id();
            $order->guitar_id = $request->guitar_id;
            $order->order_number = mt_rand(100000, 999999);
            $order->order_price = isset($dicountAmount) ? $guitar->price - $dicountAmount : $guitar->price;
            $order->order_status = Order::PENDING;
            $order->save();
            if(isset($dicountAmount)){
                $order->coupons()->attach($coupon->id);
            }
            $guitar = Guitar::findorfail($request->guitar_id);
            $guitar->reamaining_quantity = $guitar->reamaining_quantity - 1;
            $guitar->save();
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], ServerMessage::SERVER_ERROR);
        }
        return response()->json(['oder' => $order, 'message' => 'your order has been placed'], ServerMessage::SUCESS);
    }

    public function showOrder()
    {
        try {
            // $order = Order::with('guitar','user')->get();
            $order = Order::all();
            // $order = Order::where('order_status','paid')->get();
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], ServerMessage::SERVER_ERROR);
        }
        return response()->json(['order' => $order], ServerMessage::SUCESS);
    }

    public function totalSale()
    {
        $orders = Order::where('order_status', Order::PAID)->get();
        $total = 0;
        foreach ($orders as $order) {
            $total += $order->order_price;
        }
        return response()->json(['total' => $total], ServerMessage::SUCESS);
    }
}
