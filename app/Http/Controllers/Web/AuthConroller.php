<?php

namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class AuthConroller extends Controller
{
    public function login(Request $request)
    {
        // for validation
        $validator = Validator::make($request->only('email', 'password'), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            $error = $validator->errors();
            return ['error' => $error];
        }

        // login user
        try {
            $credentails = $request->only('email', 'password');

            if (!Auth::attempt($credentails)) {
                return ['message' => 'Incorrect user name and password'];
            }
            //  $token = Auth::attempt($credentails);
            // return ['Data'=>$this->responeWithToken($token),'message'=>'Login Successfull']; 
            $user = Auth::user();
            $request->session()->put('user', $user);
            return Redirect('/guitar');
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    // private function responeWithToken($token){
    //     return [
    //         'data'=>Auth::user(),
    //         'token'=>$token
    //     ];
    // }

    public function register(Request $request)
    {
        //For validation
        $request->validate([
            'name' => 'required|min:3|max:30',
            'password' => 'required',
            'email' => 'required|unique:users|email',
        ]);

        // $validator= Validator::make($request->all(),[
        //         'name' => 'required|min:3|max:30',
        //         'password' => 'required',
        //         'email' => 'required|unique:users|email',

        // ]);
        // if($validator->fails()){
          
        // return Redirect()->back()->with('errors',"ds");
        
        //     // dd($validator->errors());
        // }
    
            try {
                // save new user
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->role_type = User::NORMAL_USER;
                $user->save();

            } catch (Exception $e) {
                Session::flash('error',$e->getMessage());
            }
        return Redirect()->back();    
    }
}
