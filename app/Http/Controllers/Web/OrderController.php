<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\cart;
use App\Models\Guitar;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
     public function orderNow(){
    $userId = Session::get('user')['id'];
    $data=DB::table('carts')
     ->join('guitars','carts.guitar_id','guitars.id')
     ->where('carts.user_id',$userId) 
     ->sum('guitars.price');
     return view('ui.ordernow',compact('data'));
}

public function orderPlace(Request $request){
    $userId = Session::get('user')['id'];
    $allcart=cart::where('user_id',$userId)->get();
    foreach($allcart as $cart)
    {
        $order = new Order;
        $order->guitar_id = $cart['guitar_id'];
        $order->user_id = $cart['user_id'];
        $order->order_number = mt_rand(100000,999999);
        $order->order_status = Order::PENDING;
        $order->save();
        $guitar = Guitar::find($order->guitar_id);
        $guitar->reamaining_quantity = $guitar->reamaining_quantity-1;
        $guitar->save();

    }
    cart::where('user_id',$userId)->delete();
    return redirect('/guitar');
    }
    
    Public function myOrder(){
         $userId = Session::get('user')['id'];
        $data=DB::table('orders')
        ->join('guitars','orders.guitar_id','guitars.id')
        ->select('guitars.*','orders.*','orders.id as order_id')
        ->where('orders.user_id',$userId,)
        ->get();

        return view('ui.myorder',compact('data'));

    }
    public function removeOrders($id){
        Order::destroy($id);
        return redirect('/myorder');
    }
}