<?php

namespace App\Http\Controllers\Web\Cms;

use App\Http\Controllers\Controller;
use App\Models\Guitar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class CmsController extends Controller
{
   public function index()
   {
      $guitar = Guitar::all();
      return View('/cms/index', compact('guitar'));
   }
}
