<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGuitarRequest;
use App\Models\Guitar;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class GuitarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('UserAuth',['except'=>['index','show']]);
    }

    public function index()
    {
        $guitar = Guitar::all();
        return view('ui.product',compact('guitar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGuitarRequest $request)
    {
        // $request->validate([
        //         'name'=>'required|max:30|min:3',
        //         'type'=>'required',
        //         'description'=>'required',
        //         'Warranty'=>'required',
        //         'price'=>'required',
        //         'total_quantity'=>'required',
        //         'gallery'=>'required',
        // ]);  
        
        // store a new guitar
        try{
            $guitar = new Guitar();
            $guitar->name = $request->name;
            $guitar->type = $request->type;
            $guitar->description = $request->description;
            $guitar->Warranty = $request->Warranty;
            $guitar->price = $request->price;
            $guitar->total_quantity = $request->total_quantity;
            $guitar->reamaining_quantity = $request->total_quantity;
            $guitar->gallery = $request->gallery;
            $guitar->save();
        }catch(Exception $e){
            Session::flash('error',$e->getMessage());
        }
       return Redirect()->back();
    //    return view('ui.store',compact('guitar'));

      
       
        


    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
     $guitar = Guitar::find($id);
     return view('ui.pdetail',compact('guitar'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Guitar::destroy($id);
        return redirect('/guitar');
    }
}
