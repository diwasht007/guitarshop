<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {
        if ($request->session()->has('user')) {
            
            $cart = new cart;
            $cart->user_id = $request->session()->get('user')['id'];
            $cart->guitar_id = $request->guitar_id;
        
            $cart->save();
            return redirect('/guitar');
        } else {
            return redirect('/login');
        }
    }
    public static function cartItem()
    {
        $user_id = Session::get('user')['id'];
        return cart::where('user_id', $user_id)->count();
        // return view('ui.header',compact('total'));
    }

    public function cartList(){
    $userId = Session::get('user')['id'];
       $data= DB::table('carts')
        ->join('guitars','carts.guitar_id','guitars.id')
        ->select('guitars.*','carts.id as cart_id')
        ->where('carts.user_id',$userId) 
        ->get();
        return view('ui.cartlist',compact('data'));
    }

    public function removeCart($id){
        cart::destroy($id);
        return redirect('/cartlist');
       

    }
}
