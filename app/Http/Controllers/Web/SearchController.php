<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Guitar;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){
    $data = Guitar::where('name','like','%'.$request->input('query').'%')->get();
    return view('ui.search',compact('data'));
    }
}
