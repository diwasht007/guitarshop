<?php

namespace App\Http\Controllers\Auth;

use App\Constants\ServerMessage;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        //For validation
        $validator = Validator::make($request->all(), [
            'name'=> 'required|min:3|max:30',
            'password'=> 'required',
            'email'=> 'required|unique:users|email'
        ]);
        if ($validator->fails()){
            $error = $validator->errors();
            return response()->json(['error'=>$error], ServerMessage::VALIDATION_ERROR);
        }

    try{
        // save new user
        $user = new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->role_type=User::NORMAL_USER;
        $user->save();
    }catch(Exception $e){
        return response()->json(['error'=>$e->getMessage()], ServerMessage::SERVER_ERROR);
    }
        return response()->json(['data'=>$user, 'message'=>'User has bean saved'], ServerMessage::CREATED);
    }

    public function login(Request $request){
        // for validation
        $validator = Validator::make($request->only('email','password'),[
            'email'=>'required|email',
            'password'=>'required'
        ]);
        if($validator->fails()){
            $error = $validator->errors();
            return response()->json(['error'=>$error], ServerMessage::VALIDATION_ERROR);
        }

        // login user
        try{
            $credentails = $request->only('email', 'password');
          
            if(!Auth::guard('api')->attempt($credentails))
            {
                return response()->json(['message'=>'Incorrect user name and password'],ServerMessage::LOGIN_FAILURE);
            }
            $token = Auth::guard('api')->attempt($credentails);
            return response()->json(['Data'=>$this->responeWithToken($token),'message'=>'Login Successfull', ],ServerMessage::SUCESS);  
        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()],ServerMessage::SERVER_ERROR);

        }
    }

    private function responeWithToken($token){
        return [
            'data'=>Auth::guard('api')->user(),
            'token'=>$token
        ];
    }

    public function modifyUser(Request $request, $id){
        try{
        $user = User::findorfail($id);
        $user->role_type = $request->role_type;
        $user->save();
        }catch(Exception $e){
        return response()->json(["error"=> $e->getMessage()], ServerMessage::SERVER_ERROR);
        }
        return response()->json(['data'=>$user,'message'=>'User Sucessfully Updated'], ServerMessage::SUCESS);

    }
}
