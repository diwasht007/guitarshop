<?php

namespace App\Http\Controllers\Guitar;

use App\Constants\ServerMessage;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreGuitarRequest;
use App\Models\Guitar;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GuitarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('isAdmin',['except'=>['index','show']]);
    }

    public function index()
    {
        // show's all books
        $guitar = Guitar::all();
        return response()->json(['Data'=>$guitar],ServerMessage::SUCESS);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGuitarRequest $request)
    {
        // using validation
        // $validator = Validator::make($request->all(),[
        //     'name'=>'required|max:30|min:3',
        //     'type'=>'required',
        //     'description'=>'required',
        //     'Warranty'=>'required',
        //     'price'=>'required',
        //     'total_quantity'=>'required',
        //     'gallery'=>'required'
        // ]);
        // if($validator->fails()){
        //     $error = $validator->errors();
        //     return response()->json(['error'=>$error],ServerMessage::VALIDATION_ERROR);
        // }
        // store a new guitar
        try{
            $guitar = new Guitar();
            $guitar->name = $request->name;
            $guitar->type = $request->type;
            $guitar->description = $request->description;
            $guitar->Warranty = $request->Warranty;
            $guitar->price = $request->price;
            $guitar->total_quantity = $request->total_quantity;
            $guitar->reamaining_quantity = $request->total_quantity;
            $guitar->gallery = $request->gallery;
            $guitar->save();
        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()],ServerMessage::SERVER_ERROR);
        }
        return response()->json(['Data'=>$guitar,'message'=>'A guitar has been saved'],ServerMessage::SERVER_ERROR);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // show by id
        try{
            $guitar = Guitar::findorfail($id);
        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()], ServerMessage::SERVER_ERROR);   
        }
        return response()->json(['data'=>$guitar], ServerMessage::SUCESS);
        


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
        $guitar = Guitar::findorfail($id);
        $guitar->name = $request->name;
        $guitar->type = $request->type;
        $guitar->description = $request->description;
        $guitar->Warranty = $request->Warranty;
        $guitar->price = $request->price;
        $guitar->total_quantity = $request->total_quantity;
        $guitar->reamaining_quantity = $request->total_quantity;
        $guitar->gallery = $request->gallery;
        $guitar->save();
    }catch(Exception $e){
        return response()->json(['error'=>$e->getMessage()],ServerMessage::SERVER_ERROR);
    }
    return response()->json(['Data'=>$guitar,'message'=>' Updated Sucessfully'],ServerMessage::SERVER_ERROR);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Guitar::findorfail($id)->delete();
        }catch(Exception $e){
            return response()->json(["error"=> $e->getMessage()], ServerMessage::SERVER_ERROR);
        }
        return response()->json(['message'=>'Deleted Sucessfully'], ServerMessage::SUCESS);
    }
}
