<?php

namespace App\Http\Middleware;

use App\Constants\ServerMessage;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(!(Auth::guard('api')->user() && Auth::guard('api')->user()->role_type == User::ADMIN))
        return response()->json(['message'=>'unauthorised access'],ServerMessage::LOGIN_FAILURE); 
        return $next($request);
    }
}
