<?php

use App\Http\Controllers\Web\AuthConroller;
use App\Http\Controllers\Web\CartController;
use App\Http\Controllers\Web\Cms\CmsController;
use App\Http\Controllers\Web\GuitarController;
use App\Http\Controllers\Web\OrderController;
use App\Http\Controllers\Web\SearchController;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// using template
Route::get('/cms',[CmsController::class,'index']);




// using blade without template
Route::get('/login', function () {
    return view('ui.login');
});
Route::get('/register', function () {
    return view('ui.register');
});
Route::get('/logout', function () {
    Session::forget('user');
    return redirect('/login');
});
Route::get('/store', function () {
    return view('ui.store');
});

Route::post('/login',[AuthConroller::class,'login']);
Route::post('/register',[AuthConroller::class,'register']);

Route::get('/search',[SearchController::class,'search']);
Route::resource('/guitar',GuitarController::class);
Route::post('/addtocart',[CartController::class,'addToCart']);
Route::get('/cartlist',[CartController::class,'cartList']);
Route::get('/removecart/{id}',[CartController::class,'removeCart']);
Route::get('/ordernow',[OrderController::class,'orderNow']);
Route::post('/orderplace',[OrderController::class,'orderPlace']);
Route::get('/myorder',[OrderController::class,'myOrder']);
Route::get('/removeorder/{id}',[OrderController::class,'removeOrders']);






