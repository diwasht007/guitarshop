<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Coupon\CouponController;
use App\Http\Controllers\Guitar\GuitarController;
use App\Http\Controllers\Order\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// Auth
Route::post('register',[AuthController::class, 'register']);
Route::post('login',[AuthController::class, 'login']);
Route::post('modifyUser/{id}',[AuthController::class, 'modifyUser'])->middleware(['isAdmin:api']);

// guitar
Route::apiResource('guitar',GuitarController::class);

// Order
Route::post('placeorder',[OrderController::class,'placeorder'])->middleware(['auth:api']);
Route::get('showorder',[OrderController::class, 'showOrder'])->middleware(['auth:api']);
Route::get('totalsale',[OrderController::class, 'totalSale']);

// Coupons
Route::post('addcoupons',[CouponController::class,'addCoupons']);
Route::get('showAllCoupons',[CouponController::class,'showAllCoupons']);

// Category
Route::apiResource('category',CategoryController::class);



