<?php

namespace Database\Seeders;

use App\Models\Guitar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class guitar_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guitars')->insert([
            ['name'=>'YAMAHA',
            'type'=>'Acoustic',
            'price'=>'20000',
            'description'=>'comes with bag and a extra string',
            'Warranty'=>'2 years',
            'total_quantity'=>'15',
            'reamaining_quantity'=>'15',
            'gallery'=>'https://www.bettermusic.com.au/media/catalog/product/5/3/53f9e28387d945cb894fe0cdfd916a58_12073.jpg?optimize=high&fit=bounds&height=1200&width=1920'
            ],

            ['name'=>'FENDER',
            'type'=>'Acoustic',
            'price'=>'20000',
            'description'=>'comes with bag and a extra string',
            'Warranty'=>'2 years',
            'total_quantity'=>'15',
            'reamaining_quantity'=>'15',
            'gallery'=>'https://www.bettermusic.com.au/media/catalog/product/5/3/53f9e28387d945cb894fe0cdfd916a58_12073.jpg?optimize=high&fit=bounds&height=1200&width=1920'
            ],

            ['name'=>'FENDER',
            'type'=>'Acoustic',
            'price'=>'20000',
            'description'=>'comes with bag and a extra string',
            'Warranty'=>'2 years',
            'total_quantity'=>'15',
            'reamaining_quantity'=>'15',
            'gallery'=>'https://upload.wikimedia.org/wikipedia/commons/4/45/GuitareClassique5.png'
            ],

            ['name'=>'MAX Gigkit',
            'type'=>'Eletric',
            'price'=>'20000',
            'description'=>'comes with bag and a extra string',
            'Warranty'=>'2 years',
            'total_quantity'=>'15',
            'reamaining_quantity'=>'15',
            'gallery'=>'https://cdn.djcity.com.au/wp-content/uploads/2020/06/20174607/MAX-GigKit-Red-Stratocaster-Electric-Guitar-Pack-2.jpg'
            ],

            ['name'=>'Natural steel',
            'type'=>'Acoustic',
            'price'=>'20000',
            'description'=>'comes with bag and a extra string',
            'Warranty'=>'2 years',
            'total_quantity'=>'15',
            'reamaining_quantity'=>'15',
            'gallery'=>'https://www.artistguitars.com.au/assets/full/12700.jpg?20210318035648'
            ],

            ['name'=>'Logans',
            'type'=>'Eletric',
            'price'=>'20000',
            'description'=>'comes with bag and a extra string',
            'Warranty'=>'2 years',
            'total_quantity'=>'15',
            'reamaining_quantity'=>'15',
            'gallery'=>'https://cdn.shopify.com/s/files/1/1225/3418/products/Squier-Affinity-Stratocaster-Electric-Guitar-Laurel-Brown-Sunburst-Logans-Pianos_600x.jpg?v=1654771506'
            ],
        ]);
            
            
    }
}
